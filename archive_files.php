<?php
/**
 * Plugin Name: JF WebDesign Sucuri Patch
 * Plugin URI: https://www.jfwebdesign.com/
 * Description: This patch is a temporary fix for white screen after login, caused by Sucuri plugin. The files sucuri-oldfailedlogins.php and sucuri-auditqueue.php get too large and cause the crash. This plugin will archive the large files under a new name to allow for new logins.
 * Version: 1.0
 * Author: JF WebDesign
 * Author URI: https://www.jfwebdesign.com/
 */

function jf_check_replace_file() {
	$maxfilesize = 5000000;
	$identifier = date("Y-m-d-H-i-s");
	$upload_dir = wp_upload_dir();
	$first_file = 'sucuri-auditqueue.php';
	$second_file = 'sucuri-oldfailedlogins.php';
	
    $first_file_path  = $upload_dir['basedir'] . '/sucuri/'.$first_file;
	$second_file_path =	$upload_dir['basedir'] . '/sucuri/'.$second_file;

	$new_upload_dir = $upload_dir['basedir'];
	//$org_upload_dir = $new_upload_dir . 'sucuri';
    $full_new_upload_dir = $new_upload_dir . '/sucuri/sucuri_patch_archive';
    if (! is_dir($full_new_upload_dir)) {
       mkdir( $full_new_upload_dir, 0700 );
    }
		$auditqueuefilesize = filesize($first_file_path);
		$oldfailedloginsfilesize = filesize($second_file_path);
		if ($auditqueuefilesize > $maxfilesize) {
			rename($first_file_path, $full_new_upload_dir.'/'.$identifier . '_'.$first_file);
			
		}
		if ($oldfailedloginsfilesize > $maxfilesize) {
			rename($second_file_path, $full_new_upload_dir.'/'.$identifier . '_'.$second_file);
		
		}
	

$days = 2;  
$path = $full_new_upload_dir.'/';  
  
// Open the directory  
if ($handle = opendir($path))  
{  
    // Loop through the directory  
    while (false !== ($file = readdir($handle)))  
    {  
        // Check the file we're doing is actually a file  
        if (is_file($path.$file))  
        {  
				
            // Check if the file is older than X days old  
            if (filemtime($path.$file) < ( time() - ( $days * 24 * 60 * 60 ) ) )  
            {  
                // Do the deletion  
                unlink($path.$file);  
					//$message = $path.$file;
					//	echo "<script type='text/javascript'>alert('$message');</script>";
            }  
        }  
    }  
} 
	
}

add_action( 'wp_authenticate', 'jf_check_replace_file');
?>